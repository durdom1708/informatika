﻿using System;
using System.Windows.Forms;

namespace WinFormsApp1
{
    public partial class Enter : Form
    {
        public Enter()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Hide();
            Convertion1 frm = new Convertion1();
            frm.ShowDialog();
            Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Hide();
            Convertion2 frm = new Convertion2();
            frm.ShowDialog();
            Close();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Hide();
            Summary frm = new Summary();
            frm.ShowDialog();
            Close();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            Hide();
            Difference frm = new Difference();
            frm.ShowDialog();
            Close();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            Hide();
            Multiplication frm = new Multiplication();
            frm.ShowDialog();
            Close();
        }
    }
}
