﻿using System;
using System.Text;

namespace WinFormsApp1
{
    public static class Convertions1
    {
        const string Table = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        static int TableLength = 36;
        public static string ConvertionFrom(string Value, int FromBase)
        {
            if (FromBase >= TableLength)
            {
                return "Вы превысили возможную СС";
            }

            long Rank = 1, Result = 0;

            if (FromBase != 10)
            {
                for (var i = Value.Length - 1; i >= 0; i--)
                {
                    var Index = Table.IndexOf(Value[i]);
                    if (Index < 0 || Index >= FromBase)
                    {
                        return ("Некоторые символы не входят в систему счисления");
                    }
                    Result += Rank * Index;
                    Rank *= FromBase;
                }
                return Convert.ToString(Result);
            }
            return Value;
        }

        public static string ConvertionTo(string PValue, int ToBase)
        {
            if (ToBase >= TableLength)
            {
                return "Вы превысели возможную СС";
            }

            if (ToBase != 10)
            {
                int Value = Convert.ToInt32(PValue);
                string NewValue = "";
                while (Value > 0)
                {
                int lilIndex = Convert.ToInt32(Value % ToBase);
                NewValue = Table[lilIndex] + NewValue;
                Value = Value / ToBase;
                }
                return Convert.ToString(NewValue);
            }

            return PValue;
        }

        public static string NumberToRoman(int number)
        {
            if (number < 0 || number > 3999)
                return "значение должно быть в диапазоне 0 - 3,999.";

            if (number == 0) return "N";

            int[] values = new int[] { 1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1 };
            string[] numerals = new string[]
            { "M", "CM", "D", "CD", "C", "XC", "L", "XL", "X", "IX", "V", "IV", "I" };

            StringBuilder result = new StringBuilder();

            for (int i = 0; i < 13; i++)
            {
                while (number >= values[i])
                {
                    number -= values[i];
                    result.Append(numerals[i]);
                }
            }

            return result.ToString();
        }
    }
}
