﻿using System;
using System.Windows.Forms;

namespace WinFormsApp1
{
    public partial class Convertion2 : Form
    {
        public Convertion2()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int Number = new int();
            if (!int.TryParse(textBox1.Text, out Number))
                MessageBox.Show("Введите корректное число");
            else
                MessageBox.Show(Convertions1.NumberToRoman(Number));
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Hide();
            Enter frm = new Enter();
            frm.ShowDialog();
            Close();
        }
    }
}
