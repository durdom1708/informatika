﻿using System;
using System.Windows.Forms;

namespace WinFormsApp1
{
    public partial class Convertion1 : Form
    {
        public Convertion1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string Number = textBox1.Text;
            int FrBase = Convert.ToInt32(textBox2.Text);
            int ToBase = Convert.ToInt32(textBox3.Text);
            string FromBase = Convertions1.ConvertionFrom(Number, FrBase);
            MessageBox.Show(Convert.ToString(Convertions1.ConvertionTo(FromBase, ToBase)));
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Hide();
            Enter frm = new Enter();
            frm.ShowDialog();
            Close();
        }
    }
}
