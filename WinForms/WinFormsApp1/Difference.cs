﻿using System;
using System.Windows.Forms;

namespace WinFormsApp1
{
    public partial class Difference : Form
    {
        public Difference()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string Number1 = textBox1.Text;
            int FrBase1 = Convert.ToInt32(textBox2.Text);
            string Number2 = textBox3.Text;
            int FrBase2 = Convert.ToInt32(textBox4.Text);
            int ToBase = Convert.ToInt32(textBox5.Text);
            string Dec1 = Convertions1.ConvertionFrom(Number1, FrBase1);
            string Dec2 = Convertions1.ConvertionFrom(Number2, FrBase2);
            string Ans = Convert.ToString(Convert.ToInt32(Dec1) - Convert.ToInt32(Dec2));
            MessageBox.Show(Convert.ToString(Convertions1.ConvertionTo(Ans, ToBase)));
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Hide();
            Enter frm = new Enter();
            frm.ShowDialog();
            Close();
        }
    }
}
